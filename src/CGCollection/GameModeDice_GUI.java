package CGCollection;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class "CGCollection.GameModeDice_GUI"
 *
 * Graphical user interface for Dice Game.
 * Handles interacting with the game, and the game over screen.
 * Calls methods from GameModeDice when needed, and displays the results on screen.
 *
 * @version 1.0
 * @since 2017-12-11
 * @author James Martin
 */
public class GameModeDice_GUI implements ActionListener {

    //Declarations for elements integral to game play
    GameModeDice gmdice;
    Player player1;
    Player player2;
    Player player3;
    Player player4;
    int playerTurn = 1;

    Frontend frontend;

    //Declarations for GUI Elements
    JPanel screenDiceBoard = new JPanel();

    JButton btnTakeTurn = new JButton(" ____'s Turn");
    JLabel lblP1Name= new JLabel("Player 1: ");
    JLabel lblP1Score = new JLabel("Score: 0");
    JLabel lblP2Name = new JLabel("Player 2: ");
    JLabel lblP2Score = new JLabel("Score: 0");
    JLabel lblP3Name = new JLabel("Player 3: ");
    JLabel lblP3Score = new JLabel("Score: 0");
    JLabel lblP4Name = new JLabel("Player 4: ");
    JLabel lblP4Score = new JLabel("Score: 0");
    JLabel lblDiceLeft = new JLabel("Dice Remaining: ");
    JLabel lblCurrentRoll = new JLabel("Current roll: ");
    JLabel lblCurrentPlayerScore = new JLabel("___'s Score: ");

    //Declarations for game over gui
    JPanel screenDiceGameOver = new JPanel();

    JButton btnReturnMenu = new JButton("Return to Menu");
    JLabel lblDiceWinner = new JLabel("____ wins!");
    JLabel lblDiceLoser = new JLabel("_____ has the lowest score!");
    JLabel lblFinalScores = new JLabel("Final Scores");
    JLabel lblFinalScorep1 = new JLabel("Player 1: ");
    JLabel lblFinalScorep2 = new JLabel("Player 2: ");
    JLabel lblFinalScorep3 = new JLabel("Player 3: ");
    JLabel lblFinalScorep4 = new JLabel("Player 4: ");

    /**
     * Constructor
     * @param p1 Player 1, player
     * @param p2 Player 2, player
     * @param p3 Player 3, player
     * @param p4 Player 4, player
     * @param nOfDice Number of dice to be rolled in the game
     */
    public GameModeDice_GUI(Player p1, Player p2, Player p3, Player p4, int nOfDice, Frontend fend){
        //Configures objects used by game logic
        gmdice = new GameModeDice(nOfDice);
        player1 = p1;
        player2 = p2;
        player3 = p3;
        player4 = p4;

        frontend = fend;

        //Configures GUI Components
        screenDiceBoard.setLayout(null);

        screenDiceBoard.add(btnTakeTurn);
        btnTakeTurn.setBounds(320,580,600,100);
        btnTakeTurn.addActionListener(this);

        screenDiceBoard.add(lblP1Name);
        lblP1Name.setText("Player 1: " + player1.getPlayerName());
        lblP1Name.setBounds(0,0,240,20);

        screenDiceBoard.add(lblP1Score);
        lblP1Score.setBounds(0,20,240,20);

        screenDiceBoard.add(lblP2Name);
        lblP2Name.setText("Player 2: " + player2.getPlayerName());
        lblP2Name.setBounds(1040,0,240,20);

        screenDiceBoard.add(lblP2Score);
        lblP2Score.setBounds(1040,20,240,20);

        screenDiceBoard.add(lblP3Name);
        lblP3Name.setText("Player 3: " + player3.getPlayerName());
        lblP3Name.setBounds(0,640,240,20);

        screenDiceBoard.add(lblP3Score);
        lblP3Score.setBounds(0,660,240,20);

        screenDiceBoard.add(lblP4Name);
        lblP4Name.setText("Player 4: " + player1.getPlayerName());
        lblP4Name.setBounds(1040,640,240,20);

        screenDiceBoard.add(lblP4Score);
        lblP4Score.setBounds(1040,660,240,20);

        screenDiceBoard.add(lblDiceLeft);
        lblDiceLeft.setText("Dice Remaining: " + Integer.toString(nOfDice));
        lblDiceLeft.setBounds(500,0,240,20);

        screenDiceBoard.add(lblCurrentRoll);
        lblCurrentRoll.setBounds(500,300,240,20);

        screenDiceBoard.add(lblCurrentPlayerScore);
        lblCurrentPlayerScore.setBounds(500,320,240,20);

        //Configures game over screen GUI
        screenDiceGameOver.setLayout(null);

        screenDiceGameOver.add(btnReturnMenu);
        btnReturnMenu.setBounds(320,580,600,100);
        btnReturnMenu.addActionListener(this);

        screenDiceGameOver.add(lblDiceWinner);
        lblDiceWinner.setBounds(400,100,400,20);

        screenDiceGameOver.add(lblDiceLoser);
        lblDiceLoser.setBounds(400,200,400,20);

        screenDiceGameOver.add(lblFinalScores);
        lblFinalScores.setBounds(400,220,240,20);

        screenDiceGameOver.add(lblFinalScorep1);
        lblFinalScorep1.setBounds(400,240,240,20);

        screenDiceGameOver.add(lblFinalScorep2);
        lblFinalScorep2.setBounds(400,260,240,20);

        screenDiceGameOver.add(lblFinalScorep3);
        lblFinalScorep3.setBounds(400,280,240,20);

        screenDiceGameOver.add(lblFinalScorep4);
        lblFinalScorep4.setBounds(400,300,240,20);
    }

    /**
     * Handles player 1's turn, and updates GUI with results.
     */
    private void p1Turn()
    {
        //Takes player's turn
        Dice dice = new Dice();
        int pRoll = dice.rollDice();
        int score = gmdice.takeTurn(player1.getDiceGameScore(), pRoll);
        player1.setDiceGameScore(score);

        //Updates GUI with new scores
        lblCurrentRoll.setText("Current roll: " + Integer.toString(pRoll));
        lblP1Score.setText("Score: " + Integer.toString(player1.getDiceGameScore()));
        lblCurrentPlayerScore.setText(player1.getPlayerName() + "'s Score: " + Integer.toString(player1.getDiceGameScore()));
        //Sets game UI ready for next player to take turn
        btnTakeTurn.setText(player2.getPlayerName() + "'s turn");
        playerTurn = 2;
    }

    /**
     * Handles player 2's turn, and updates GUI with results.
     */
    private void p2Turn()
    {
        //Takes player's turn
        Dice dice = new Dice();
        int pRoll = dice.rollDice();
        int score = gmdice.takeTurn(player2.getDiceGameScore(), pRoll);
        player2.setDiceGameScore(score);

        //Updates GUI with new scores
        lblCurrentRoll.setText("Current roll: " + Integer.toString(pRoll));
        lblP2Score.setText("Score: " + Integer.toString(player2.getDiceGameScore()));
        lblCurrentPlayerScore.setText(player2.getPlayerName() + "'s Score: " + Integer.toString(player2.getDiceGameScore()));
        //Sets game UI ready for next player to take turn
        btnTakeTurn.setText(player3.getPlayerName() + "'s turn");
        playerTurn = 3;
    }

    /**
     * Handles player 3's turn, and updates GUI with results.
     */
    private void p3Turn()
    {//Takes player's turn
        Dice dice = new Dice();
        int pRoll = dice.rollDice();
        int score = gmdice.takeTurn(player3.getDiceGameScore(), pRoll);
        player3.setDiceGameScore(score);

        //Updates GUI with new scores
        lblCurrentRoll.setText("Current roll: " + Integer.toString(pRoll));
        lblP3Score.setText("Score: " + Integer.toString(player3.getDiceGameScore()));
        lblCurrentPlayerScore.setText(player3.getPlayerName() + "'s Score: " + Integer.toString(player3.getDiceGameScore()));
        //Sets game UI ready for next player to take turn
        btnTakeTurn.setText(player4.getPlayerName() + "'s turn");
        playerTurn = 4;
    }

    /**
     * Handles player 4's turn, and updates GUI with results.
     */
    private void p4Turn()
    {
        Dice dice = new Dice();
        int pRoll = dice.rollDice();
        int score = gmdice.takeTurn(player4.getDiceGameScore(), pRoll);
        player4.setDiceGameScore(score);

        //Updates GUI with new scores
        lblCurrentRoll.setText("Current roll: " + Integer.toString(pRoll));
        lblP4Score.setText("Score: " + Integer.toString(player4.getDiceGameScore()));
        lblCurrentPlayerScore.setText(player4.getPlayerName() + "'s Score: " + Integer.toString(player4.getDiceGameScore()));

        //Reduces dice left and checks if game is over
        gmdice.reduceDiceLeft();
        lblDiceLeft.setText("Dice Remaining: " + Integer.toString(gmdice.getDiceLeft()));
        //Ends game if no dice are left
        if (gmdice.getDiceLeft() == 0) {
            gameEnd();
        }else {
            //Sets game UI ready for next player to take turn
            btnTakeTurn.setText(player1.getPlayerName() + "'s turn");
            playerTurn = 1;
        }
    }

    /**
     * Called when game is over, gets final scores and displays game over screen.
     */
    private void gameEnd()
    {
        frontend.setGUI("dice game over");

        //Gets high score from game mode
        int[] scores = {player1.getDiceGameScore(), player2.getDiceGameScore(), player3.getDiceGameScore(), player4.getDiceGameScore()};
        int winscore = gmdice.checkWin(scores);
        switch (winscore) {
            case 1:
                player1.setScoreWinDice();
                lblDiceWinner.setText(player1.getPlayerName() + " wins!");
                break;

            case 2:
                player2.setScoreWinDice();
                lblDiceWinner.setText(player2.getPlayerName() + " wins!");
                break;

            case 3:
                player3.setScoreWinDice();
                lblDiceWinner.setText(player3.getPlayerName() + " wins!");
                break;

            case 4:
                player4.setScoreWinDice();
                lblDiceWinner.setText(player4.getPlayerName() + " wins!");
                break;
        }

        //Gets lowest score from game mode
        int losescore = gmdice.checkLose(scores);
        switch (losescore) {
            case 1:
                player1.setScoreLoseDice();
                lblDiceLoser.setText(player1.getPlayerName() + " has the lowest score!");
                break;
            case 2:
                player2.setScoreLoseDice();
                lblDiceLoser.setText(player2.getPlayerName() + " has the lowest score!");
                break;
            case 3:
                player3.setScoreLoseDice();
                lblDiceLoser.setText(player3.getPlayerName() + " has the lowest score!");
                break;
            case 4:
                player4.setScoreLoseDice();
                lblDiceLoser.setText(player4.getPlayerName() + " has the lowest score!");
                break;
        }
        //Updates GUI
        lblFinalScorep1.setText(player1.getPlayerName() + " : " + Integer.toString(player1.getDiceGameScore()));
        lblFinalScorep2.setText(player2.getPlayerName() + " : " + Integer.toString(player2.getDiceGameScore()));
        lblFinalScorep3.setText(player3.getPlayerName() + " : " + Integer.toString(player3.getDiceGameScore()));
        lblFinalScorep4.setText(player4.getPlayerName() + " : " + Integer.toString(player4.getDiceGameScore()));

        //Updates player scores
        player1.setDiceGameScore(0);
        player2.setDiceGameScore(0);
        player3.setDiceGameScore(0);
        player4.setDiceGameScore(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        //Take turn button
        if (e.getSource() == btnTakeTurn) {
            if (playerTurn == 1) {
                p1Turn();
            } else if (playerTurn == 2) {
                p2Turn();
            } else if (playerTurn == 3) {
                p3Turn();
            } else if (playerTurn == 4) {
                p4Turn();
            }
        }

        //Return to menu after game end
        if(e.getSource() == btnReturnMenu) {
            frontend.setGUI("menu");
        }
    }
}
