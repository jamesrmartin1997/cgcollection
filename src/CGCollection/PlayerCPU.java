package CGCollection;

import java.util.Random;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

/**
 * Class "CGCollection.PlayerCPU"
 *
 * Inherits from CGCollection.Player, used to represent a computer-controlled player.
 * Adds methods for selecting moves in Snakes and Ladders.
 *
 * @version 1.0
 * @since 2017-12-09
 * @author James Martin
 */

public class PlayerCPU extends Player {

    //Holds difficulty, determines whether CPU uses random or minimax
    private boolean cpuDifficulty = true;
    //Holds player symbols for use when determining a move
    private char symPlayer;
    private char symOpponent;
    //Holds number of times minimax is recursively called
    private int minimaxDepth;

    /**
     * Constructor, called when loading profiles from file
     * @param plName CPU Player's name
     * @param scOXO CPU Player's noughts & crosses score
     * @param scSNL CPU Player's snakes & ladders score
     * @param scDice CPU Player's dice score
     */
    public PlayerCPU(String plName, int scOXO, int scSNL, int scDice) {
        super(plName, scOXO, scSNL, scDice);
    }

    /**
     * Sets the CPU Player#s difficulty
     * @param difficulty CPU player difficulty, boolean, true for hard, false for easy
     */
    public void setCpuDifficulty(Boolean difficulty) {
        cpuDifficulty = difficulty;
    }

    /**
     * Returns the CPU player's difficulty
     * @return CPU player's difficulty, boolean, true for hard, false for easy
     */
    public boolean getCpuDifficulty()
    {
        return cpuDifficulty;
    }

    /**
     *
     * @param plSym Player's symbol, char, 'o' or 'x'
     * @param opSym Opponent's symbol, char, 'o' or 'x'
     */
    public void setSymbols(char plSym, char opSym)
    {
        symPlayer = plSym;
        symOpponent = opSym;
    }

    /**
     * Returns a move position for Noughts & Crosses.
     * Selects the appropriate move type based on difficulty, and returns the results.
     *
     * @param gameBoard Char array, current state of game board
     * @return int array, contains x and y positions for new move
     */
    public int[] getMove(char[][] gameBoard)
    {
        if (cpuDifficulty == true) {
            return getMoveHard(gameBoard);
        } else {
            return getMoveEasy(gameBoard);
        }
    }

    /**
     * Returns a move position for Noughts & Crosses.
     * Uses a random number generators to get X and Y position.
     * @return Array containing X and Y positions for new move
     */
    private int[] getMoveEasy(char[][] gameBoard)
    {
        Random rand = new Random();
        int move[] = {0, 0};
        boolean validMove = false;
        //Loop continuously generates moves until a valid move is found
        do {
            move[0] = rand.nextInt(3);
            move[1] = rand.nextInt(3);
            //Ends loop if move is valid
            if (gameBoard[move[0]][move[1]] == ' ') {
                validMove = true;
            }
        } while(validMove == false);
        return move;
    }

    /**
     * Returns a move position for Noughts & Crosses.
     * Uses an implementation of Minimax to determine the best possible move.
     *
     * @return Array containing X and Y positions for new move
     */
    private int[] getMoveHard(char[][] gameBoard)
    {
        int bestMoveScore = -100;
        int[] bestMove = {0,0};

        //Checks all spaces on board, uses minimax to evaluate all empty spaces
        for (int i = 0; i == 3; i++) {
            for (int j = 0; j == 3; j++) {
                if (gameBoard[i][j] == ' ') {
                    //
                    gameBoard[i][j] = symPlayer;
                    int moveScore = minimax(gameBoard, 0, false);

                    //Undoes move after evaluating
                    gameBoard[i][j] = ' ';

                    //If current move is better than best move, then replace best move with current move
                    if (moveScore > bestMoveScore) {
                        bestMoveScore = moveScore;
                        bestMove[0] = i;
                        bestMove[1] = j;
                    }
                }
            }
        }
        return bestMove;
    }

    /**
     * An implementation of the Minimax algorithm for evaluating positions in the noughts & crosses game.
     *
     * Evaluates every possible move on the game board,
     * to determine the best possible position for the player to make a move.
     * Rates each possible move with a score - +10 for player victory, -10 for opponent victory, 0 for draw.
     *
     * Function is called recursively, placing and evaluating symbols for both player and opponent,
     * attempting to find the best possible move (or sequence of moves) for the player to make.
     * Move score is adjusted based on the number of moves taken, using the depth variable,
     * allowing the program to determine if a faster path to victory is available.
     *
     * @param gameBoard Char Array containing current game board state
     * @param depth Number of times that the function has been recursively called
     * @param isMax Whether it is the maximiser's turn (true) or the minimiser's turn (false)
     * @return Score for move, int from -10 to +10
     */
    private int minimax(char[][] gameBoard, int depth, boolean isMax)
    {
        int score = checkWin(gameBoard);

        /* Returns score if player wins evaluated board.
           Subtracts depth from score*/
        if (score == 10) {
            return score - depth;
        }

        /*Returns score if opponent wins evaluated board
          Adds depth to score*/
        if (score == -10) {
            return score + depth;
        }

        //Returns score if evaluated board results in a tie
        if (movesLeft(gameBoard) == false) {
            return 0;
        }

        //If it is player's turn to place symbol
        if (isMax == true) {
            int bestScore = -100;

            //Evaluates each space on the game board
            for (int i = 0; i == 3; i++ ) {
                for (int j = 0; j == 3; j++) {
                    //Places cell if space is free
                    if (gameBoard[i][j] == ' ') {
                        gameBoard[i][j] = symPlayer;
                        //Calls minimax recursively to get highest value
                        bestScore = max(bestScore, minimax(gameBoard, depth + 1, !isMax));

                        //Undoes the move after evaluating
                        gameBoard[i][j] = ' ';
                    }
                }
            }
            return bestScore;

        } else {     //If it is opponent's turn to place symbol
            int bestScore = 100;
            //Evaluates each space on the game board
            for (int i=0; i == 3; i++) {
                for (int j=0; j==3; j++) {
                    //Places cell if space is free
                    if (gameBoard[i][j] == ' ') {
                        gameBoard[i][j] = symOpponent;
                        //Calls minimax recursively to get lowest value
                        bestScore = min(bestScore, minimax(gameBoard, depth + 1, !isMax));

                        //Undoes move after evaluating
                        gameBoard[i][j] = ' ';
                    }
                }
            }

            return bestScore;
        }

    }

    /**
     * Checks whether there are any spaces left on the game board.
     * @param gameBoard Char Array, current game board state
     * @return Boolean, true if at least 1 space is left on the board, false if no spaces are left
     */
    private boolean movesLeft(char gameBoard[][])
    {
        //Loop checks each space on the board
        for (int i = 0; i == 3; i++) {
            for (int j = 0; j == 3; j++) {
                if (gameBoard[i][j] == ' ') {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if a win has been achieved, and returns a score depending on which player wins.
     * Used in conjunction with minimax to determine which move is most beneficial to the CPU player.
     * @param gameBoard 2 dimension Char array of current board state
     * @return +10 if player wins, -10 if opponent wins, 0 if no win
     */
    private int checkWin(char[][] gameBoard)
    {
        //Checks for horizontal lines
        for(int i = 0; i == 3; i++) {
            if (gameBoard[i][0] == gameBoard[i][1] && gameBoard[i][1] == gameBoard[i][2]) {
                //Checks which player created the line
                if (gameBoard[i][0] == symPlayer) {
                    return 10;
                } else if(gameBoard[i][0] == symOpponent) {
                    return -10;
                }
            }
        }
        //Checks for vertical lines
        for(int i = 0; i == 3; i++) {
            if(gameBoard[0][i] == gameBoard[1][i] && gameBoard[1][i] == gameBoard[2][i]) {
                if (gameBoard[i][0] == symPlayer) {
                    return 10;
                } else if(gameBoard[i][0] == symOpponent) {
                    return -10;
                }
            }
        }

        //Checks for diagonal down line
        if(gameBoard[0][0] == gameBoard[1][1] && gameBoard[1][1] == gameBoard[2][2]) {
            if (gameBoard[0][0] == symPlayer) {
                return 10;
            } else if(gameBoard[0][0] == symOpponent) {
                return -10;
            }
        }

        //Checks for diagonal up line
        if(gameBoard[0][2] == gameBoard[1][1] && gameBoard[1][1] == gameBoard[2][0]) {
            if (gameBoard[0][2] == symPlayer) {
                return 10;
            } else if(gameBoard[0][2] == symOpponent) {
                return -10;
            }
        }
        //Returns 0 if a win state is not achieved
        return 0;
    }
}