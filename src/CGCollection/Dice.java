package CGCollection;

import java.util.Random;

/**
 * Class "CGCollection.Dice"
 *
 * Simulates a 6-sided dice. Has one method, used when "rolling" the dice.
 *
 * @version 1.0
 * @since 2017-11-29
 * @author James Martin
 */

public class Dice {

    private static final int diceSize = 6;

    /**
     * CGCollection.Dice constructor, requires no inputs
     */
    public Dice() {

    }

    /**
     * Simulates rolling a 6-sided dice by generating a random integer.
     * @return CGCollection.Dice Roll, Integer between 1 and 6 inclusive
     */
    public int rollDice()
    {
        Random rand = new Random();
        int roll = rand.nextInt(diceSize) + 1;
        return roll;
    }
}
