package CGCollection;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class "CGCollection.GameModeOXO_GUI"
 *
 * AAA
 *
 * @version 1.0
 * @since 2017-12-xx
 * @author James Martin
 */
public class GameModeOXO_GUI implements ActionListener{

    //
    GameModeOXO oxo = new GameModeOXO();
    private char playerSymbol = 'O';
    Player player1;
    int p1Index;
    Player player2;
    int p2Index;
    Frontend frontend;

    //Declarations for Game Screen GUI elements
    JPanel screenOXOBoard =  new JPanel();

    JLabel lblTurnCounter = new JLabel("Turn 1");

    JButton btnBoard00 = new JButton(" ");
    JButton btnBoard01 = new JButton(" ");
    JButton btnBoard02 = new JButton(" ");
    JButton btnBoard10 = new JButton(" ");
    JButton btnBoard11 = new JButton(" ");
    JButton btnBoard12 = new JButton(" ");
    JButton btnBoard20 = new JButton(" ");
    JButton btnBoard21 = new JButton(" ");
    JButton btnBoard22 = new JButton(" ");
    JButton btnStartTurn = new JButton("Take Turn");

    JLabel lblPlayer1Name = new JLabel("Player 1: ");
    JLabel lblPlayer1Score = new JLabel("Score: ");
    JLabel lblPlayer2Name = new JLabel("Player 2: ");
    JLabel lblPlayer2Score = new JLabel("Score: ");

    //Declarations for Game Over screen GUI elements
    JPanel screenOXOGameOver = new JPanel();
    JButton btnReturnToMenu = new JButton("Return to Menu");
    JLabel lblGameWinner = new JLabel("____ is the winner!");
    JLabel lblGameEndP1Name = new JLabel("Player 1: ");
    JLabel lblGameEndP1Score = new JLabel("Score: ");
    JLabel lblGameEndP2Name = new JLabel("Player 2: ");
    JLabel lblGameEndP2Score = new JLabel("Score: ");

    public GameModeOXO_GUI(Player p1, int p1No, Player p2, int p2No, Frontend fend) {
        //Configures gameplay elements
        player1 = p1;
        p1Index = p1No;
        player2 = p2;
        p2Index = p2No;

        frontend = fend;

        //Initialises GUI elements
        screenOXOBoard.setLayout(null);

        screenOXOBoard.add(lblTurnCounter);
        lblTurnCounter.setBounds(620,0,100,40);

        screenOXOBoard.add(btnBoard00);
        btnBoard00.setBounds(340,40,200,200);
        btnBoard00.setEnabled(false);
        btnBoard00.addActionListener(this);

        screenOXOBoard.add(btnBoard01);
        btnBoard01.setBounds(540,40,200,200);
        btnBoard01.setEnabled(false);
        btnBoard01.addActionListener(this);

        screenOXOBoard.add(btnBoard02);
        btnBoard02.setBounds(740,40,200,200);
        btnBoard02.setEnabled(false);
        btnBoard02.addActionListener(this);

        screenOXOBoard.add(btnBoard10);
        btnBoard10.setBounds(340,240,200,200);
        btnBoard10.setEnabled(false);
        btnBoard10.addActionListener(this);

        screenOXOBoard.add(btnBoard11);
        btnBoard11.setBounds(540,240,200,200);
        btnBoard11.setEnabled(false);
        btnBoard11.addActionListener(this);

        screenOXOBoard.add(btnBoard12);
        btnBoard12.setBounds(740,240,200,200);
        btnBoard12.setEnabled(false);
        btnBoard12.addActionListener(this);

        screenOXOBoard.add(btnBoard20);
        btnBoard20.setBounds(340,440,200,200);
        btnBoard20.setEnabled(false);
        btnBoard20.addActionListener(this);

        screenOXOBoard.add(btnBoard21);
        btnBoard21.setBounds(540,440,200,200);
        btnBoard21.setEnabled(false);
        btnBoard21.addActionListener(this);

        screenOXOBoard.add(btnBoard22);
        btnBoard22.setBounds(740,440,200,200);
        btnBoard22.setEnabled(false);
        btnBoard22.addActionListener(this);

        screenOXOBoard.add(btnStartTurn);
        btnStartTurn.setBounds(340,640,600,40);
        btnStartTurn.addActionListener(this);

        screenOXOBoard.add(lblPlayer1Name);
        lblPlayer1Name.setBounds(10,640,240,40);
        lblPlayer1Name.setText(String.join(" ", "Player 1:", player1.getPlayerName()));

        screenOXOBoard.add(lblPlayer1Score);
        lblPlayer1Score.setBounds(10,660,240,40);
        lblPlayer1Score.setText(String.join(" ", "Score:", Integer.toString(player1.getScoreOXO())));

        screenOXOBoard.add(lblPlayer2Name);
        lblPlayer2Name.setBounds(1000,640,240,40);
        lblPlayer2Name.setText(String.join(" ", "Player 2:", player2.getPlayerName()));

        screenOXOBoard.add(lblPlayer2Score);
        lblPlayer2Score.setBounds(1000,660,240,40);
        lblPlayer2Score.setText(String.join(" ", "Score:", Integer.toString(player2.getScoreOXO())));

        //Configures Game Over screen GUI elements
        screenOXOGameOver.setLayout(null);

        screenOXOGameOver.add(btnReturnToMenu);
        btnReturnToMenu.addActionListener(this);
        btnReturnToMenu.setBounds(320,580,600,100);

        screenOXOGameOver.add(lblGameWinner);
        lblGameWinner.setBounds(400, 100, 240, 20);

        screenOXOGameOver.add(lblGameEndP1Name);
        lblGameEndP1Name.setBounds(400, 160, 240, 20);

        screenOXOGameOver.add(lblGameEndP1Score);
        lblGameEndP1Score.setBounds(400, 180, 240, 20);

        screenOXOGameOver.add(lblGameEndP2Name);
        lblGameEndP2Name.setBounds(400,220,240,20);

        screenOXOGameOver.add(lblGameEndP2Score);
        lblGameEndP2Score.setBounds(400, 240, 240,20);
    }

    /**
     * Disables buttons used for placing symbols on the game board.
     */
    private void disableButtons()
    {
        btnBoard00.setEnabled(false);
        btnBoard01.setEnabled(false);
        btnBoard02.setEnabled(false);
        btnBoard10.setEnabled(false);
        btnBoard11.setEnabled(false);
        btnBoard12.setEnabled(false);
        btnBoard20.setEnabled(false);
        btnBoard21.setEnabled(false);
        btnBoard22.setEnabled(false);
    }

    /**
     * Enables buttons used for placing symbols on the game board.
     * Only enables buttons if the cell they represent is still empty.
     */
    private void enableButtons()
    {
        if (btnBoard00.getText() == " ") {
            btnBoard00.setEnabled(true);
        }
        if (btnBoard01.getText() == " ") {
            btnBoard01.setEnabled(true);
        }
        if (btnBoard02.getText() == " ") {
            btnBoard02.setEnabled(true);
        }
        if (btnBoard10.getText() == " ") {
            btnBoard10.setEnabled(true);
        }
        if (btnBoard11.getText() == " ") {
            btnBoard11.setEnabled(true);
        }
        if (btnBoard12.getText() == " ") {
            btnBoard12.setEnabled(true);
        }
        if (btnBoard20.getText() == " ") {
            btnBoard20.setEnabled(true);
        }
        if (btnBoard21.getText() == " ") {
            btnBoard21.setEnabled(true);
        }
        if (btnBoard22.getText() == " ") {
            btnBoard22.setEnabled(true);
        }
    }

    /**
     * Changes player symbol between turns.
     */
    private void setPlayerSymbol()
    {
        if (playerSymbol == 'O') {
            playerSymbol = 'X';
        } else if (playerSymbol == 'X') {
            playerSymbol = 'O';
        }
    }

    /**
     * Displays GUI at end of game.
     * Updates high score board, game log and player information
     * @param endType char, represents winning player, O for p1, X for p2
     */
    private void gameEnd(char endType)
    {
        if (endType == 'O') {
            player1.setScoreWinOXO();
            player2.setScoreLoseOXO();
            lblGameWinner.setText(player1.getPlayerName() + " is the winner!");
            frontend.gamelog.addGameLog("OXO", player1.getPlayerName(), player1.getScoreOXO(), player2.getPlayerName(), player2.getScoreOXO());
        } else if (endType == 'X') {
            player2.setScoreWinOXO();
            player1.setScoreLoseOXO();
            lblGameWinner.setText(player2.getPlayerName() + " is the winner!");
            frontend.gamelog.addGameLog("OXO", player2.getPlayerName(), player2.getScoreOXO(), player1.getPlayerName(), player1.getScoreOXO());
        }
        //Updates high score board
        frontend.highscores.updateScoreBoardOXO(player1.getPlayerName(), player1.getScoreOXO());
        frontend.highscores.updateScoreBoardOXO(player2.getPlayerName(), player2.getScoreOXO());

        //Updates profiles in profileList
        frontend.profileList.set(p1Index, player1);
        frontend.profileList.set(p2Index, player2);

        //Saves profiles, scores, and game logs to file
        FileIO io = new FileIO();
        io.writeProfile(frontend.profileList);
        io.writeHighScore(frontend.highscores.getHighScoreOXOSave(), "OXO");
        io.writeGameLog(frontend.gamelog.getGameLog());

        //Displays game over screen
        frontend.setGUI("oxo game over");
        lblGameEndP1Name.setText("Player 1: " + player1.getPlayerName());
        lblGameEndP1Score.setText("Score: " + Integer.toString(player1.getScoreOXO()));
        lblGameEndP2Name.setText("Player 2: " + player2.getPlayerName());
        lblGameEndP2Score.setText("Score: " + Integer.toString(player2.getScoreOXO()));
    }

    public void actionPerformed(ActionEvent e)
    {

        /* Clicked when starting turn
         * Checks which symbol should be taking a turn
         * Then checks if the player is Human or CPU
         * If CPU, the game takes turn automatically
         * Otherwise, the game lets human player take their turn*/
        if(e.getSource() == btnStartTurn) {
            if(playerSymbol == 'O') {
                if (player1 instanceof PlayerCPU) {
                    int[] move = ((PlayerCPU) player1).getMove(oxo.getGameBoard());
                    oxo.setSymbol(move[0], move[1], playerSymbol);
                    setPlayerSymbol();
                } else {
                    enableButtons();
                }
            } else if (playerSymbol == 'X') {
                if (player2 instanceof  PlayerCPU) {
                    int[] move = ((PlayerCPU) player2).getMove(oxo.getGameBoard());
                    oxo.setSymbol(move[0], move[1], playerSymbol);
                    setPlayerSymbol();
                }else {
                    enableButtons();
                }
            }
        }

        //Game Board Buttons, places symbols when clicked
        if(e.getSource() == btnBoard00) {
            //Sets symbol and displays on game board
            oxo.setSymbol(0, 0, playerSymbol);
            btnBoard00.setText(String.valueOf(oxo.getSymbol(0,0)));
            //Ends game if win or draw has occurred
            char endType = oxo.checkWin();
            if (endType != ' ') {
                btnStartTurn.setEnabled(false);
                gameEnd(endType);
            }
            //Changes player symbol and prevents more symbols from being added until new turn begins
            setPlayerSymbol();
            disableButtons();
            //Updates turn counter on gui
            lblTurnCounter.setText(String.join(" ","Turn", Integer.toString(oxo.getTurnCounter())));
        }
        if(e.getSource() == btnBoard01) {
            oxo.setSymbol(0, 1, playerSymbol);
            btnBoard01.setText(String.valueOf(oxo.getSymbol(0,1)));
            //Ends game if win or draw has occurred
            char endType = oxo.checkWin();
            if (endType != ' ') {
                btnStartTurn.setEnabled(false);
                gameEnd(endType);
            }
            //Changes player symbol and prevents more symbols from being added until new turn begins
            setPlayerSymbol();
            disableButtons();
            //Updates turn counter on gui
            lblTurnCounter.setText(String.join(" ","Turn", Integer.toString(oxo.getTurnCounter())));
        }

        if(e.getSource() == btnBoard02) {
            oxo.setSymbol(0, 2, playerSymbol);
            btnBoard02.setText(String.valueOf(oxo.getSymbol(0,2)));
            //Ends game if win or draw has occurred
            char endType = oxo.checkWin();
            if (endType != ' ') {
                btnStartTurn.setEnabled(false);
                gameEnd(endType);
            }
            //Changes player symbol and prevents more symbols from being added until new turn begins
            setPlayerSymbol();
            disableButtons();
            //Updates turn counter on gui
            lblTurnCounter.setText(String.join(" ","Turn", Integer.toString(oxo.getTurnCounter())));
        }

        if(e.getSource() == btnBoard10) {
            oxo.setSymbol(1, 0, playerSymbol);
            btnBoard10.setText(String.valueOf(oxo.getSymbol(1,0)));
            //Ends game if win or draw has occurred
            char endType = oxo.checkWin();
            if (endType != ' ') {
                btnStartTurn.setEnabled(false);
                gameEnd(endType);
            }
            //Changes player symbol and prevents more symbols from being added until new turn begins
            setPlayerSymbol();
            disableButtons();
            //Updates turn counter on gui
            lblTurnCounter.setText(String.join(" ","Turn", Integer.toString(oxo.getTurnCounter())));

        }
        if(e.getSource() == btnBoard11) {
            oxo.setSymbol(1, 1, playerSymbol);
            btnBoard11.setText(String.valueOf(oxo.getSymbol(1,1)));
            //Ends game if win or draw has occurred
            char endType = oxo.checkWin();
            if (endType != ' ') {
                btnStartTurn.setEnabled(false);
                gameEnd(endType);
            }
            //Changes player symbol and prevents more symbols from being added until new turn begins
            setPlayerSymbol();
            disableButtons();
            //Updates turn counter on gui
            lblTurnCounter.setText(String.join(" ","Turn", Integer.toString(oxo.getTurnCounter())));

        }
        if(e.getSource() == btnBoard12) {
            oxo.setSymbol(1, 2, playerSymbol);
            btnBoard12.setText(String.valueOf(oxo.getSymbol(1,2)));
            //Ends game if win or draw has occurred
            char endType = oxo.checkWin();
            if (endType != ' ') {
                btnStartTurn.setEnabled(false);
                gameEnd(endType);
            }
            //Changes player symbol and prevents more symbols from being added until new turn begins
            setPlayerSymbol();
            disableButtons();
            //Updates turn counter on gui
            lblTurnCounter.setText(String.join(" ","Turn", Integer.toString(oxo.getTurnCounter())));

        }
        if(e.getSource() == btnBoard20) {
            oxo.setSymbol(2, 0, playerSymbol);
            btnBoard20.setText(String.valueOf(oxo.getSymbol(2,0)));
            //Ends game if win or draw has occurred
            char endType = oxo.checkWin();
            if (endType != ' ') {
                btnStartTurn.setEnabled(false);
                gameEnd(endType);
            }
            //Changes player symbol and prevents more symbols from being added until new turn begins
            setPlayerSymbol();
            disableButtons();
            //Updates turn counter on gui
            lblTurnCounter.setText(String.join(" ","Turn", Integer.toString(oxo.getTurnCounter())));

        }
        if(e.getSource() == btnBoard21) {
            oxo.setSymbol(2, 1, playerSymbol);
            btnBoard21.setText(String.valueOf(oxo.getSymbol(2,1)));
            //Ends game if win or draw has occurred
            char endType = oxo.checkWin();
            if (endType != ' ') {
                btnStartTurn.setEnabled(false);
                gameEnd(endType);
            }
            //Changes player symbol and prevents more symbols from being added until new turn begins
            setPlayerSymbol();
            disableButtons();
            //Updates turn counter on gui
            lblTurnCounter.setText(String.join(" ","Turn", Integer.toString(oxo.getTurnCounter())));

        }
        if(e.getSource() == btnBoard22) {
            oxo.setSymbol(2, 2, playerSymbol);
            btnBoard22.setText(String.valueOf(oxo.getSymbol(2,2)));
            //Ends game if win or draw has occurred
            char endType = oxo.checkWin();
            if (endType != ' ') {
                btnStartTurn.setEnabled(false);
                gameEnd(endType);
            }
            //Changes player symbol and prevents more symbols from being added until new turn begins
            setPlayerSymbol();
            disableButtons();
            //Updates turn counter on gui
            lblTurnCounter.setText(String.join(" ","Turn", Integer.toString(oxo.getTurnCounter())));

        }
    }
}
