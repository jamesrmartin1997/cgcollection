package CGCollection;

import java.util.Arrays;
import java.util.Collections;

/**
 * Class "CGCollection.GameModeDice"
 *
 * This class contains the logic to play a simple dice game.
 * Players take it in turns to roll the dice until no more dice are left.
 * The winner is the player that scored the most points.
 *
 * @version 1.0
 * @since 2017-12-03
 * @author James Martin
 */

public class GameModeDice {

    //Holds number of dice left for each player
    private int diceLeft;

    /**
     * Constructor. Takes the user's desired number of dice.
     * @param nOfDice int, number of dice that each player should roll in the game
     */
    public GameModeDice(int nOfDice) {
        diceLeft = nOfDice;
    }

    /**
     * Gets number of dice left
     * @return int, number of dice left
     */
    public int getDiceLeft()
    {
        return diceLeft;
    }

    /**
     * Reduces the number of dice left by 1.
     * To be called after every player has taken one turn.
     */
    public void reduceDiceLeft()
    {
        diceLeft -= 1;
    }

    /**
     * Adds a dice roll to a player's score. Used when a player takes their turn, after a dice roll.
     * @param playerScore int, A player's current score before the turn
     * @param diceRoll int, CGCollection.Player's dice roll
     * @return int, player's new score after dice roll
     */
    public int takeTurn(int playerScore, int diceRoll)
    {
        playerScore += diceRoll;
        return playerScore;
    }

    /**
     * Checks which player has won the game.
     *
     * Sorts an array containing player scores into descending order.
     * The player at the top of the array will have the highest score, and is returned by this method.
     *
     * Only call after diceleft = 0.
     * @param pScores 2D array containing player scores in col 1, and player numbers in col 2
     * @return Winning player
     */
    public int checkWin(int[][] pScores)
    {
        //Sort array into descending order
        Arrays.sort(pScores, Collections.reverseOrder());
        return pScores[0][1];
    }

}
