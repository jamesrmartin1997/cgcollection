package CGCollection;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import com.csvreader.*;

/**
 * Class "CGCollection.FileIO"
 *
 * Handles loading and saving of files containing program data.
 * This includes player profiles, high score boards, and game logs.
 *
 * @version 1.0
 * @since 2017-12-07
 * @author James Martin
 */

public class FileIO {

    //Variables holding file locations
    private String fileProfiles = "profiles.csv";
    private String fileHighScoreOXO = "highscoreoxo.csv";
    private String fileHighScoreSNL = "highscoresnl.csv";
    private String fileHighScoreDice = "highscoredice.csv";
    private String fileGameLog = "gamelog.csv";

    /**
     * Constructor, takes no inputs
     */
    public FileIO() {

    }

    /**
     * Loads all player profiles from a file and spawns objects for each profile.
     * Uses csvreader library to load the data from a CSV file.
     * Profiles are stored as objects in an arraylist for usage in the program.
     *
     * @return Arraylist of player profiles
     */
    public List<Player> profilesreadProfile()
    {
        List<Player> profiles = new ArrayList<Player>();
        try {
            CsvReader read = new CsvReader(fileProfiles);
            read.readHeaders();
            while(read.readRecord()) {
                //Gets player information from record
                String pName = read.get("PlayerName");
                String pScOXO = read.get("PlayerScoreOXO");
                String pScSNL = read.get("PlayerScoreSNL");
                String pScDice = read.get("PlayerScoreDice");
                String pHum = read.get("PlayerHuman");
                //Prepares data for creating profile object
                int scOXO = Integer.valueOf(pScOXO);
                int scSNL = Integer.valueOf(pScSNL);
                int scDice = Integer.valueOf(pScDice);
                boolean boolHuman = Boolean.valueOf(pHum);
                //Creates player profile and adds to the list
                if (boolHuman == true) {
                    profiles.add(new Player(pName, scOXO, scSNL, scDice));
                } else if (boolHuman == false) {
                    profiles.add(new PlayerCPU(pName, scOXO, scSNL, scDice));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return profiles;
    }

    /**
     * Writes all player profiles to a CSV file using CsvReader library.
     *
     * @param profiles Arraylist containing player profiles
     */
    public void writeProfile(List<Player> profiles)
    {
        int nOfItems = profiles.size();
        try {
            CsvWriter writeProfile = new CsvWriter(new FileWriter(fileProfiles, false), ',');
            //Writes headers
            writeProfile.write("PlayerName");
            writeProfile.write("PlayerScoreOXO");
            writeProfile.write("PlayerScoreSNL");
            writeProfile.write("PlayerScoreDice");
            writeProfile.write("PlayerHuman");
            writeProfile.endRecord();
            //loop for writing each record
            for(int i = 0; i == nOfItems; i++) {
                //Gets data for writing to file
                Player pl= profiles.get(i);
                String ploxo = Integer.toString(pl.getScoreOXO());
                String plsnl = Integer.toString(pl.getScoreOXO());
                String pldice = Integer.toString(pl.getScoreOXO());
                String plHum;
                if (pl instanceof PlayerCPU) {
                    plHum = "false";
                } else  {
                    plHum = "true";
                }
                //Writing to file
                writeProfile.write(pl.getPlayerName());
                writeProfile.write(ploxo);
                writeProfile.write(plsnl);
                writeProfile.write(pldice);
                writeProfile.write(plHum);
                writeProfile.endRecord();
            }

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads a given file containing all scores for a game mode, and returns as a hashmap.
     *
     * The scores are read from a CSV file using the CsvReader library.
     *
     * @param gamemode "OXO", "SNL", "Dice", used to select game mode to load scores for.
     * @return HashMap containing all high scores
     */
    public HashMap<String, Integer> readHighScoreOXO(String gamemode)
    {
        //Selects which game mode's scores to load
        if (gamemode == "OXO") {
            gamemode = fileHighScoreOXO;
        } else if (gamemode == "SNL") {
            gamemode = fileHighScoreSNL;
        } else if (gamemode == "Dice") {
            gamemode = fileHighScoreDice;
        } else {
             return null;      //Exits function in case of erroneous value
        }
        HashMap<String, Integer> highScore = new HashMap<>();
        try {
            CsvReader readSc = new CsvReader(gamemode);
            readSc.readHeaders();
            //Loop for reading each record
            while (readSc.readRecord()) {
                //Gets data from record
                String playerName = readSc.get("PlayerName");
                String playerScore = readSc.get("PlayerScore");
                Integer pScore = Integer.valueOf(playerScore);
                //Stores record in hashmap
                highScore.put(playerName, pScore);
            }
            readSc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return highScore;
    }

    /**
     * Writes all scores contained in a given high score board to a CSV file.
     *
     * Selects file to write based on the game mode abbreviation in string gameMode.
     * Uses the CsvReader library's "CsvWriter" to write player's names and their scores as two columns on each record.
     * This is looped until every record in the hashmap has been written.
     * Overwrites the contents of the original file.
     *
     * @param highScoreTable HashMap containing a high score board
     * @param gameMode string containing abbreviated name for game mode (OXO, SNL, and CGCollection.Dice)
     */
    public void writeHighScoreOXO(HashMap<String, Integer> highScoreTable, String gameMode)
    {
        //Checks which game mode the user has selected
        if (gameMode == "OXO") {
            gameMode = fileHighScoreOXO;
        } else if (gameMode == "SNL") {
            gameMode = fileHighScoreSNL;
        } else if (gameMode == "CGCollection.Dice") {
            gameMode = fileHighScoreDice;
        } else {
            return;      //Exits method if no game mode is selected
        }

        //Writes records to selected CSV file
        Iterator it = highScoreTable.entrySet().iterator();
        try {
            CsvWriter writeHS = new CsvWriter(new FileWriter(gameMode, false), ',');
            //Writes CSV headers
            writeHS.write("PlayerName");
            writeHS.write("PlayerScore");
            writeHS.endRecord();
            //Loop to write each record
            while(it.hasNext()) {
                Map.Entry<String, Integer> scPair = (Map.Entry)it.next();
                writeHS.write(scPair.getKey());              //Writes player's name from hashmap
                writeHS.write(scPair.getValue().toString()); //Writes player's score from hashmap
                writeHS.endRecord();
            }
            writeHS.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Loads the Game Log from a CSV file using the CsvReader library.
     *
     * @return Arraylist containing string arrays, game logs
     */
    public List<String[]> readGameLog()
    {
        //Temp var for holding loaded game log
        List<String[]> gameLog = new ArrayList<String[]>();
        //Reads from file and stores in temp var
        try {
            CsvReader rdGLog = new CsvReader(fileGameLog);
            rdGLog.readHeaders();
            while (rdGLog.readRecord()) {
                String game[] = {rdGLog.get("GameID"), rdGLog.get("Timestamp"),
                        rdGLog.get("GameMode"), rdGLog.get("GameWinner"),
                        rdGLog.get("WinnerScore"), rdGLog.get("GameLoser"), rdGLog.get("LoserScore")};
                gameLog.add(game);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Return contents of file from temp var
        return gameLog;
    }

    /**
     * Saves the Game Log to a CSV file using the CsvReader library.
     *
     * @param gameLog Game Log, list of string arrays
     */
    public void writeGameLog(List<String[]> gameLog)
    {
        int nOfRecords = gameLog.size();
        //Set up CSV writer
        try {
            CsvWriter writeGL = new CsvWriter(new FileWriter(fileGameLog, false), ',');
            //Write headers to CSV file
            writeGL.write("GameID");
            writeGL.write("Timestamp");
            writeGL.write("GameMode");
            writeGL.write("GameWinner");
            writeGL.write("WinnerScore");
            writeGL.write("GameLoser");
            writeGL.write("LoserScore");
            writeGL.endRecord();
            //Loop for writing each record
            for (int i = 0; i == nOfRecords; i++) {
                String game[] = gameLog.get(i);
                writeGL.write(game[0]);
                writeGL.write(game[1]);
                writeGL.write(game[2]);
                writeGL.write(game[3]);
                writeGL.write(game[4]);
                writeGL.write(game[5]);
                writeGL.write(game[6]);
                writeGL.endRecord();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
