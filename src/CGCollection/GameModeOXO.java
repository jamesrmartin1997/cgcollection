package CGCollection;

/**
 * Class "CGCollection.GameModeOXO"
 * Simulates a game of Noughts & Crosses.
 * Contains methods for altering the game board, and checking if a win condition has been met.
 *
 * @version 1.1
 * @since 2017-12-07
 * @author James Martin s1607397
 */

public class GameModeOXO {

    private char[][] gameBoard = new char[3][3];
    private int turnCounter = 1;

    /**
     * Class constructor. Does not require any inputs.
     */
    public GameModeOXO() {

    }

    /**
     * Places a player's symbol on the game board using a given x and y position.
     * Also increments the turn counter.
     *
     * @param xpos Selected X position
     * @param ypos Selected Y position
     * @param playerSymbol Symbol to show which player claims the space, either 'O' or 'X'.
     */
    public void setSymbol(int xpos, int ypos, char playerSymbol)
    {
        gameBoard[xpos][ypos] = playerSymbol;
        turnCounter++;
    }

    /**
     * Gets the character at a given position of the game board.
     *
     * @param xpos X position on game board
     * @param ypos Y position on game board
     * @return  Character at specified X and Y position on game board
     */
    public char getSymbol(int xpos, int ypos)
    {
        return gameBoard[xpos][ypos];
    }

    /**
     *
     * @return Turn Counter, integer
     */
    public int getTurnCounter()
    {
        return turnCounter;
    }

    /**
     * Gets the entire game board, used by AI for minimax method.
     * @return Game board, 2 dimension char array
     */
    public char[][] getGameBoard()
    {
        return gameBoard;
    }

    /**
     * Checks whether a player has achieved a win.
     *
     * Checks each possible type of line that a player can achieve.
     * If a player achieves a win, their symbol is returned to represent a win. (e.g. a O win returns a 'O')
     * If the grid is full, a 'D' is returned, to signify a draw.
     * If no player achieves a win, a space is returned, signifying that
     *
     * @return Char to represent winning player. 'O' or 'X' for players, 'D' if draw, and ' ' if no current winner.
     */
    public char checkWin()
    {
        //Check horizontal lines
        char hor = checkHorizontal();
        if (hor == 'O') {
            return 'O';
        } else if (hor == 'X') {
            return 'X';
        }

        //Check vertical lines
        char ver = checkVertical();
        if (ver == 'O') {
            return 'O';
        } else if (ver == 'X') {
            return 'X';
        }

        //Check diagonal lines
        char diagUp = checkDiagonalUp();
        if (diagUp == 'O') {
            return 'O';
        } else if(diagUp == 'X') {
            return 'X';
        }

        char diagDown = checkDiagonalDown();
        if (diagDown == 'O') {
            return 'O';
        } else if (diagDown == 'X') {
            return 'X';
        }

        //Ends game if grid is full
        if (turnCounter == 10) {
            return 'D';
        }

        //Returns if no player wins
        return ' ';
    }

    /**
     * Checks each horizontal line, to see if a player has made a horizontal line.
     * @return Char to represent winning player. 'O' for player 1, 'X' for player 2, ' ' for no win.
     */
    private char checkHorizontal()
    {
        for (int xpos = 0; xpos < 3; xpos++) {
            if (gameBoard[xpos][0] == gameBoard[xpos][1] && gameBoard[xpos][1] == gameBoard[xpos][2]) {
                return gameBoard[xpos][0];
            }
        }
        return ' ';
    }

    /**
     * Checks each vertical line, to see if a player has made a vertical line.
     * @return Char to represent winning player. 'O' for player 1, 'X' for player 2, ' ' for no win.
     */
    private char checkVertical()
    {
        for (int ypos = 0; ypos < 3; ypos++) {
            if (gameBoard[0][ypos] == gameBoard[1][ypos] && gameBoard[1][ypos] == gameBoard[2][ypos]) {
                return gameBoard[0][ypos];
            }
        }
        return ' ';
    }

    /**
     * Checks a diagonal line from the bottom-left to top-right corner, to see if a player has made a line.
     * @return Char to represent winning player. 'O' for player 1, 'X' for player 2, ' ' for no win.
     */
    private char checkDiagonalUp()
    {
        if (gameBoard[0][2] == gameBoard[1][1] && gameBoard[1][1] == gameBoard[2][0]) {
            return gameBoard[0][2];
        }
        return ' ';
    }

    /**
     * Checks a diagonal line from the top-left to bottom-right corner, to see if a player has made a line.
     * @return Char to represent winning player. 'O' for player 1, 'X' for player 2, ' ' for no win.
     */
    private char checkDiagonalDown()
    {
        if (gameBoard[0][0] == gameBoard[1][1] && gameBoard[1][1] == gameBoard[2][2]) {
            return gameBoard[0][0];
        }
        return ' ';
    }

}
