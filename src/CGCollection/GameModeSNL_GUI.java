package CGCollection;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 */
public class GameModeSNL_GUI implements ActionListener{

    //Declarations for gameplay elements
    GameModeSNL gmsnl;
    Player player1;
    Player player2;
    Player player3;
    Player player4;
    Frontend frontend;
    int playerTurn = 1;

    //Declarations for gameplay screen GUI
    JPanel screenSNLGame = new JPanel();
    JButton btnTakeTurn = new JButton("____'s Turn");
    JLabel lblP1Name = new JLabel("Player 1: ");
    JLabel lblP1Pos = new JLabel("Position: 0");
    JLabel lblP1Score = new JLabel("Score: ");
    JLabel lblP2Name = new JLabel("Player 2: ");
    JLabel lblP2Pos = new JLabel("Position: 0");
    JLabel lblP2Score = new JLabel("Score: ");
    JLabel lblP3Name = new JLabel("Player 3: ");
    JLabel lblP3Pos = new JLabel("Position: 0");
    JLabel lblP3Score = new JLabel("Score: ");
    JLabel lblP4Name = new JLabel("Player 4: ");
    JLabel lblP4Pos = new JLabel("Position: 0");
    JLabel lblP4Score = new JLabel("Score: ");

    JLabel lblGameStatus = new JLabel("Game Status");
    JLabel lblGameStatus2 = new JLabel("");

    //Declarations for game over screen GUI
    JPanel screenSNLGameOver = new JPanel();
    JButton btnReturnToMenu = new JButton("Return to Menu");
    JLabel lblSNLWinner = new JLabel("___is the winner!");
    JLabel lblSNLLoser = new JLabel("___ is the loser, only reaching space ___!");
    JLabel lblP1GameEnd = new JLabel("____'s Score: ");
    JLabel lblP2GameEnd = new JLabel("____'s Score: ");
    JLabel lblP3GameEnd = new JLabel("____'s Score: ");
    JLabel lblP4GameEnd = new JLabel("____'s Score: ");

    /**
     * Constructor
     * @param p1 Player 1, player
     * @param p2 Player 2, player
     * @param p3 Player 3, player
     * @param p4 Player 4, player
     * @param bouncerule Sets whether bounce rule is used in game, boolean
     */
    public GameModeSNL_GUI(Player p1, Player p2, Player p3, Player p4, boolean bouncerule, Frontend fend) {
        //Configures gameplay elements
        gmsnl = new GameModeSNL(bouncerule);
        player1 = p1;
        player2 = p2;
        player3 = p3;
        player4 = p4;
        frontend = fend;

        //Configures gameplay screen GUI
        screenSNLGame.setLayout(null);

        screenSNLGame.add(btnTakeTurn);
        btnTakeTurn.setBounds(320,580,600,100);
        btnTakeTurn.addActionListener(this);
        btnTakeTurn.setText(player1.getPlayerName() + "'s turn");

        screenSNLGame.add(lblP1Name);
        lblP1Name.setBounds(0,0,240,20);
        lblP1Name.setText("Player 1: " + player1.getPlayerName());

        screenSNLGame.add(lblP1Pos);
        lblP1Pos.setBounds(0,20,240,20);

        screenSNLGame.add(lblP1Score);
        lblP1Score.setBounds(0,40,240,20);
        lblP1Score.setText("Score: " + player1.getScoreSNL());

        screenSNLGame.add(lblP2Name);
        lblP2Name.setBounds(1040,0,240,20);
        lblP2Name.setText("Player 2: " + player2.getPlayerName());

        screenSNLGame.add(lblP2Pos);
        lblP2Pos.setBounds(1040,20,240,20);

        screenSNLGame.add(lblP2Score);
        lblP2Score.setBounds(1040,40,240,20);
        lblP2Score.setText("Score: " + player2.getScoreSNL());

        screenSNLGame.add(lblP3Name);
        lblP3Name.setBounds(0,580,240,20);
        lblP3Name.setText("Player 3: " + player3.getPlayerName());

        screenSNLGame.add(lblP3Pos);
        lblP3Pos.setBounds(0,600,240,20);

        screenSNLGame.add(lblP3Score);
        lblP3Score.setBounds(0,620,240,20);
        lblP3Score.setText("Score: " + player3.getScoreSNL());

        screenSNLGame.add(lblP4Name);
        lblP4Name.setBounds(1040,580,240,20);
        lblP4Name.setText("Player 4: " + player4.getPlayerName());

        screenSNLGame.add(lblP4Pos);
        lblP4Pos.setBounds(1040,600,240,20);

        screenSNLGame.add(lblP4Score);
        lblP4Score.setBounds(1040,620,240,20);
        lblP4Score.setText("Score: " + player4.getScoreSNL());

        screenSNLGame.add(lblGameStatus);
        lblGameStatus.setBounds(400,100,400,20);

        screenSNLGame.add(lblGameStatus2);
        lblGameStatus2.setBounds(400,120,400,20);

        //Configures game over screen GUI
        screenSNLGameOver.setLayout(null);

        screenSNLGameOver.add(btnReturnToMenu);
        btnReturnToMenu.setBounds(320,580,600,100);
        btnReturnToMenu.addActionListener(this);

        screenSNLGameOver.add(lblSNLWinner);
        lblSNLWinner.setBounds(400,100,240,20);

        screenSNLGameOver.add(lblSNLLoser);
        lblSNLLoser.setBounds(400,140,240,20);

        screenSNLGameOver.add(lblP1GameEnd);
        lblP1GameEnd.setBounds(400,180,240,20);

        screenSNLGameOver.add(lblP2GameEnd);
        lblP2GameEnd.setBounds(400,200,240,20);

        screenSNLGameOver.add(lblP3GameEnd);
        lblP3GameEnd.setBounds(400,220,240,20);

        screenSNLGameOver.add(lblP4GameEnd);
        lblP4GameEnd.setBounds(400,240,240,20);
    }

    /**
     * Handles player 1 taking a turn.
     * Involves rolling dice, checking if player has landed on a snake or ladder,
     * setting position, and updating GUI elements.
     */
    private void p1Turn()
    {
        //Takes turn
        Dice dice = new Dice();
        int pMove = dice.rollDice();
        int plPos = gmsnl.takeTurn(player1.getBoardPosSNL(), pMove);
        //Alters position if player lands on a snake or ladder
        int altpos = gmsnl.landSnake(plPos);
        altpos = gmsnl.landLadder(plPos);
        if (altpos > plPos) {  //Called if player landed on a ladder
            plPos = altpos;
            lblGameStatus2.setText(player1.getPlayerName() + " has landed on a ladder!");
        } else if (altpos < plPos) { //Called if player landed on a snake
            plPos = altpos;
            lblGameStatus2.setText(player1.getPlayerName() + " has landed on a snake!");
        }
        player1.setBoardPosSNL(plPos);
        //Checks if player has reached the end of the game board
        if (gmsnl.checkWin(player1.getBoardPosSNL()) == true) {
            gameEnd(1);
        } else {
            playerTurn = 2;
        }
        //Updates GUI
        lblP1Pos.setText("Position: " + Integer.toString(plPos));
        lblGameStatus.setText(player1.getPlayerName() + " has moved to space " + plPos);
        btnTakeTurn.setText(player2.getPlayerName() + "'s turn");
    }

    /**
     * Handles player 2 taking a turn.
     * Involves rolling dice, checking if player has landed on a snake or ladder,
     * setting position, and updating GUI elements.
     */
    private void p2Turn()
    {
        //Takes turn
        Dice dice = new Dice();
        int pMove = dice.rollDice();
        int plPos = gmsnl.takeTurn(player2.getBoardPosSNL(), pMove);
        //Alters position if player lands on a snake or ladder
        int altpos = gmsnl.landSnake(plPos);
        altpos = gmsnl.landLadder(plPos);
        if (altpos > plPos) {  //Called if player landed on a ladder
            plPos = altpos;
            lblGameStatus2.setText(player2.getPlayerName() + " has landed on a ladder!");
        } else if (altpos < plPos) { //Called if player landed on a snake
            plPos = altpos;
            lblGameStatus2.setText(player2.getPlayerName() + " has landed on a snake!");
        }
        player2.setBoardPosSNL(plPos);
        //Checks if player has reached the end of the game board
        if (gmsnl.checkWin(player2.getBoardPosSNL()) == true) {
            gameEnd(2);
        } else {
            playerTurn = 3;
        }
        //Updates GUI
        lblP2Pos.setText("Position: " + Integer.toString(plPos));
        lblGameStatus.setText(player2.getPlayerName() + " has moved to space " + plPos);
        btnTakeTurn.setText(player3.getPlayerName() + "'s turn");
    }

    /**
     * Handles player 3 taking a turn.
     * Involves rolling dice, checking if player has landed on a snake or ladder,
     * setting position, and updating GUI elements.
     */
    private void p3Turn()
    {
        //Takes turn
        Dice dice = new Dice();
        int pMove = dice.rollDice();
        int plPos = gmsnl.takeTurn(player3.getBoardPosSNL(), pMove);
        //Alters position if player lands on a snake or ladder
        int altpos = gmsnl.landSnake(plPos);
        altpos = gmsnl.landLadder(plPos);
        if (altpos > plPos) {  //Called if player landed on a ladder
            plPos = altpos;
            lblGameStatus2.setText(player3.getPlayerName() + " has landed on a ladder!");
        } else if (altpos < plPos) { //Called if player landed on a snake
            plPos = altpos;
            lblGameStatus2.setText(player3.getPlayerName() + " has landed on a snake!");
        }
        player1.setBoardPosSNL(plPos);
        //Checks if player has reached the end of the game board
        if (gmsnl.checkWin(player3.getBoardPosSNL()) == true) {
            gameEnd(3);
        } else {
            playerTurn = 4;
        }
        //Updates GUI
        lblP3Pos.setText("Position: " + Integer.toString(plPos));
        lblGameStatus.setText(player3.getPlayerName() + " has moved to space " + plPos);
        btnTakeTurn.setText(player4.getPlayerName() + "'s turn");
    }

    /**
     * Handles player 4 taking a turn.
     * Involves rolling dice, checking if player has landed on a snake or ladder,
     * setting position, and updating GUI elements.
     */
    private void p4Turn()
    {
        //Takes turn
        Dice dice = new Dice();
        int pMove = dice.rollDice();
        int plPos = gmsnl.takeTurn(player4.getBoardPosSNL(), pMove);
        //Alters position if player lands on a snake or ladder
        int altpos = gmsnl.landSnake(plPos);
        altpos = gmsnl.landLadder(plPos);
        if (altpos > plPos) {  //Called if player landed on a ladder
            plPos = altpos;
            lblGameStatus2.setText(player4.getPlayerName() + " has landed on a ladder!");
        } else if (altpos < plPos) { //Called if player landed on a snake
            plPos = altpos;
            lblGameStatus2.setText(player4.getPlayerName() + " has landed on a snake!");
        }
        player4.setBoardPosSNL(plPos);
        //Checks if player has reached the end of the game board
        if (gmsnl.checkWin(player4.getBoardPosSNL()) == true) {
            gameEnd(4);
        } else {
            playerTurn = 1;
        }
        //Updates GUI
        lblP4Pos.setText("Position: " + Integer.toString(plPos));
        lblGameStatus.setText(player4.getPlayerName() + " has moved to space " + plPos);
        btnTakeTurn.setText(player4.getPlayerName() + "'s turn");
    }

    /**
     * Decides game's winner and loser, awards score accordingly, and displays
     * player scores on the GUI at the end of the game.
     * @param winner int, player number of winning player, 1 2 3 or 4
     */
    private void gameEnd(int winner)
    {
        //Sets winner's score and displays on GUI
        switch (winner) {
            case 1:
                player1.setScoreWinSNL();
                lblSNLWinner.setText(player1.getPlayerName() + " is the winner!");
                break;

            case 2:
                player2.setScoreWinSNL();
                lblSNLWinner.setText(player2.getPlayerName() + " is the winner!");
                break;

            case 3:
                player3.setScoreWinSNL();
                lblSNLWinner.setText(player3.getPlayerName() + " is the winner!");
                break;

            case 4:
                player4.setScoreWinSNL();
                lblSNLWinner.setText(player4.getPlayerName() + " is the winner!");
                break;
        }
        //Checks which player has the lowest score, and displays as part of game over screen
        int loseScore = player1.getBoardPosSNL();
        int losePlayer = 1;
        if (player2.getBoardPosSNL() < loseScore)
        {
            loseScore = player2.getBoardPosSNL();
            losePlayer = 2;
        }
        if (player3.getBoardPosSNL() < loseScore)
        {
            loseScore = player3.getBoardPosSNL();
            losePlayer = 3;
        }
        if (player4.getBoardPosSNL() < loseScore)
        {
            loseScore = player4.getBoardPosSNL();
            losePlayer = 4;
        }
        switch (losePlayer) {
            case 1:
                player1.setScoreLoseSNL();
                lblSNLLoser.setText(player1.getPlayerName() + " is the loser, only reaching space " + loseScore);
                break;

            case 2:
                player2.setScoreLoseSNL();
                lblSNLLoser.setText(player2.getPlayerName() + " is the loser, only reaching space " + loseScore);
                break;

            case 3:
                player3.setScoreLoseSNL();
                lblSNLLoser.setText(player3.getPlayerName() + " is the loser, only reaching space " + loseScore);
                break;

            case 4:
                player4.setScoreLoseSNL();
                lblSNLLoser.setText(player4.getPlayerName() + " is the loser, only reaching space " + loseScore);
                break;
        }
        //Displays player scores on game end screen
        lblP1GameEnd.setText(player1.getPlayerName() + "'s score: " + player1.getScoreSNL());
        lblP2GameEnd.setText(player2.getPlayerName() + "'s score: " + player2.getScoreSNL());
        lblP3GameEnd.setText(player3.getPlayerName() + "'s score: " + player3.getScoreSNL());
        lblP4GameEnd.setText(player4.getPlayerName() + "'s score: " + player4.getScoreSNL());

        //Switch to game over screen
        frontend.cardGUI.add(screenSNLGameOver,"snl game over");
        frontend.setGUI("snl game over");
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        //Selects correct player and takes turn, when clicked
        if(e.getSource() == btnTakeTurn) {
            switch (playerTurn) {
                case 1:
                    p1Turn();
                    break;

                case 2:
                    p2Turn();
                    break;

                case 3:
                    p3Turn();
                    break;

                case 4:
                    p4Turn();
                    break;
            }
        }

        //Returns to main menu when clicked
        if(e.getSource() == btnReturnToMenu) {
            frontend.setGUI("menu");
        }
    }
}
