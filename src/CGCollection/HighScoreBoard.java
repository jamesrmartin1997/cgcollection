package CGCollection;

import java.lang.String;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class "CGCollection.HighScoreBoard"
 *
 * Used to represent the high score boards for each game mode.
 * The boards hold one score for each player.
 * Contains methods for adding/updating scores in the board, and for getting the top 10 scores.
 *
 * @version 1.0
 * @since 2017-12-04
 * @author James Martin
 */

public class HighScoreBoard {

    //HashMaps to hold high scores for each game mode
    private HashMap<String, Integer> highScoreOXO;
    private HashMap<String, Integer> highScoreSNL;
    private HashMap<String, Integer> highScoreDice;

    public HighScoreBoard(HashMap<String, Integer> hsOXO, HashMap<String, Integer> hsSNL, HashMap<String, Integer> hsDice) {
        highScoreOXO = hsOXO;
        highScoreSNL = hsSNL;
        highScoreDice = hsDice;
    }

    /**
     * Gets a hashmap containing all scores and names.
     *
     * Used when saving all scores to a file.
     *
     * @return Hashmap, Noughts & Crosses high scores
     */
    public HashMap getHighScoreOXOSave()
    {
        return highScoreOXO;
    }

    /**
     * Gets a hashmap containing all scores and names.
     *
     * Used when saving all scores to a file.
     *
     * @return Hashmap, Snakes & Ladders high scores
     */
    public HashMap getHighScoreSNLSave()
    {
        return highScoreSNL;
    }

    /**
     * Gets a hashmap containing all scores and names.
     *
     * Used when saving all scores to a file.
     *
     * @return Hashmap, CGCollection.Dice game high scores
     */
    public HashMap getHighScoreDiceSave()
    {
        return highScoreDice;
    }

    /**
     * Gets the 10 highest scores from the Noughts & Crosses high score table in descending order.
     *
     * Sorts the hashmap containing high scores into descending order by using a stream.
     * The top 10 entries are then stored in a map and returned by the method.
     *
     * @return Map containing 10 highest scores in order, alongside player names
     */
    public Map getHighScoreOXO()
    {
        Map<String,Integer> scores = highScoreOXO.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .limit(10)
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        return scores;
    }

    /**
     * Gets the 10 highest scores from the Snakes & Ladders high score table in descending order.
     *
     * Sorts the hashmap containing high scores into descending order by using a stream.
     * The top 10 entries are then stored in a map and returned by the method.
     *
     * @return Map containing 10 highest scores in order, alongside player names
     */
    public Map getHighScoreSNL()
    {
        Map<String,Integer> scores = highScoreSNL.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(10)
                .collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        return scores;
    }

    /**
     * Gets the 10 highest scores from the CGCollection.Dice game high score table in descending order.
     *
     * Sorts the hashmap containing high scores into descending order by using a stream.
     * The top 10 entries are then stored in a map and returned by the method.
     *
     * @return Map containing 10 highest scores in order, alongside player names
     */
    public Map getHighScoreDice()
    {
        Map<String,Integer> scores = highScoreDice.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(10)
                .collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        return scores;
    }

    /**
     * Creates or updates a player's score on the Noughts & Crosses scoreboard.
     * @param playerName Name of player who set score
     * @param playerScore CGCollection.Player's Nouughts & Crosses score
     */
    public void updateScoreBoardOXO(String playerName, int playerScore)
    {
       highScoreOXO.put(playerName, playerScore);
    }

    /**
     * Creates or updates a player's score on the Snakes & Ladders scoreboard.
     * @param playerName Name of player who set score
     * @param playerScore CGCollection.Player's Snakes & Ladders score
     */
    public void updateScoreBoardSNL(String playerName, int playerScore)
    {
        highScoreSNL.put(playerName, playerScore);
    }

    /**
     * Creates or updates a player's score on the CGCollection.Dice game scoreboard.
     * @param playerName Name of player who set score
     * @param playerScore CGCollection.Player's CGCollection.Dice game score
     */
    public void updateScoreBoardDice(String playerName, int playerScore)
    {
        highScoreDice.put(playerName, playerScore);
    }

    /**
     * Removes all records for a given player from all high score boards.
     * Use only when deleting a player's profile.
     * @param playerName Name of player whose records are being deleted
     */
    public void deleteScores(String playerName)
    {
        highScoreOXO.remove(playerName);
        highScoreSNL.remove(playerName);
        highScoreDice.remove(playerName);
    }
}
