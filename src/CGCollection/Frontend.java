package CGCollection;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Class "CGCollection.Frontend"
 *
 * AAA
 *
 * @version 1.0
 * @since 2017-12-xx
 * @author James Martin
 */

public class Frontend implements ActionListener{

    //Variable declarations

    //Constructor
    public Frontend() {

    }

    //Main
    public static void main(String[] args) {
        //Load from files

        //Initialise gui
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
