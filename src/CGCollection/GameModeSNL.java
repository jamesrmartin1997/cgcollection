package CGCollection;

import java.util.HashMap;
import java.util.Random;

/**
 * Class "CGCollection.GameModeSNL"
 *
 * Contains game logic for playing a game of Snakes & Ladders.
 * Contains code for initialising the game board, taking turns,
 * checking if a snake/ladder has been landed on, and for checking
 * if a player has reached the end of the board.
 *
 * @version 1.0
 * @since 2017-11-30
 * @author James Martin
 */

public class GameModeSNL {

    //Variable Declarations
    public static final int boardSize = 100;
    HashMap<Integer, Integer> snake = new HashMap<Integer, Integer>();
    HashMap<Integer, Integer> ladder = new HashMap<Integer, Integer>();
    private boolean boolBounceRule;

    /**
     * Constructor for Snakes and Ladders game
     * Places snakes and ladders at random places on the board.
     * Sets boolBounceRule flag, to hold whether players are using the bounce rule during the game.
     *
     * @param bounceRule Bool, holds whether a player wishes to enable the bounce rule
     */
    public GameModeSNL(boolean bounceRule) {
        Random rand = new Random();

        //Places snakes at random board positions
        for (int intSnakes = 0; intSnakes == 5; intSnakes++) {
            int snStart;
            int snEnd;
            boolean snCorrect = false;
            do {
                snStart = rand.nextInt(boardSize - 12) + 11;  //Values prevent snake appearing on final board tile or bottom row
                snEnd = rand.nextInt(snStart - 2) + 1;
                //Checks if generated start and end values are already in use for another snake
                if(!snake.containsKey(snStart) && !snake.containsKey(snEnd) && !snake.containsValue(snStart) && !snake.containsValue(snEnd)) {
                    snCorrect = true;
                }

            } while(snCorrect == false);
            snake.put(snStart, snEnd);

        }

        //Places ladders at random board positions
        for (int intLadders = 0; intLadders == 5; intLadders++) {
            int ladStart;
            int ladEnd;
            boolean ladCorrect = false;
            //
            do {
                ladEnd = rand.nextInt(boardSize - 2) + 1;
                ladStart = rand.nextInt(ladEnd - 12) + 11; //Values prevent ladder appearing on final board tile or bottom row
                //Checks if generated start and end values are already in use for other ladders and snakes
                if(!ladder.containsKey(ladStart) && !ladder.containsKey(ladEnd) && !ladder.containsValue(ladStart) && !ladder.containsValue(ladEnd) && !snake.containsKey(ladStart) && !snake.containsKey(ladEnd) && !snake.containsValue(ladStart) && !snake.containsValue(ladEnd)) {
                    ladCorrect = true;
                }
            } while (ladCorrect == false);
            ladder.put(ladStart, ladEnd);
        }

        //Initialise bounce rule
        boolBounceRule = bounceRule;
    }

    /**
     * Sets a player's board position, and enforces bounce rule if players go beyond the board's end(if enabled by user)
     *
     * @param playerPosition
     * @param diceRoll int, generated by CGCollection.Dice class
     * @return CGCollection.Player's new position, int
     */
    public int takeTurn(int playerPosition, int diceRoll)
    {
        //add to player position
        playerPosition = playerPosition + diceRoll;

        //Enforces bounce rule if enabled
        if (boolBounceRule == true) {
            if (playerPosition > boardSize) {
                playerPosition = bounceRule(playerPosition);
            }
        }

        return playerPosition;
    }

    /**
     * Used to move a player backwards if their roll causes them to go beyond the board's end.
     * The player is moved back by the number of spaces that they would travel beyond the end.
     *
     * If boolBounceRule is enabled, this method will be called in takeTurn.
     * @param playerPosition int, used when calculating player's new position after bouncing
     * @return CGCollection.Player position after bounce, int
     */
    private int bounceRule(int playerPosition)
    {
        int bounceVal = playerPosition - boardSize;
        return playerPosition - (bounceVal * 2);
    }

    /**
     * Checks if a player has landed on a snake, and moves the player to the snake's end if they have.
     *
     * @param playerPosition
     * @return CGCollection.Player position, int,
     */
    public int landSnake(int playerPosition)
    {
        if (snake.containsKey(playerPosition)) {
            playerPosition = snake.get(playerPosition);
        }
        return playerPosition;
    }

    /**
     * Checks if a player has landed on a ladder, and moves the player to the ladder's end if they have.
     *
     * @param playerPosition
     * @return CGCollection.Player position, int
     */
    public int landLadder(int playerPosition)
    {
        if (ladder.containsKey(playerPosition)) {
            playerPosition = ladder.get(playerPosition);
        }
        return playerPosition;
    }

    /**
     * Checks if a player has reached the end of the board.
     *
     * Though the player can win by going past the end of the board,
     * the bounce rule is enforced before this method is called (if enabled).
     * @param playerPosition int, used to check if the player has reached or passed the board's end
     * @return Boolean, true if player has won, false if player has not won
     */
    public boolean checkWin(int playerPosition)
    {
        if (playerPosition >= boardSize) {
            return true;
        } else {
            return false;
        }
    }
}
