package CGCollection;

import java.lang.String;

/**
 * CGCollection.Player Class
 *
 * Holds statistics about a player, such as their name and scores.
 * Contains methods to set and get player scores for each game mode.
 *
 * @version 1.1
 * @since 2017-12-03
 * @author James Martin
 */

public class Player {

    //Variable Declarations
    private String playerName;
    private int scoreOXO;  //Hold scores for each mode that carry between games
    private int scoreSNL;
    private int scoreDice;

    //Variables used during gameplay
    private int boardPosSNL;
    private int diceGameScore;

    /**
     * CGCollection.Player Constructor, used when first creating a profile
     *
     * @param plName CGCollection.Player's name, String
     */
    public Player(String plName) {
        scoreOXO = 0;
        scoreSNL = 0;
        scoreDice = 0;
    }

    /**
     * CGCollection.Player Constructor used when Loading a profile
     *
     * @param plName CGCollection.Player's name, String
     * @param scOXO  CGCollection.Player's noughts & crosses score
     * @param scSNL  CGCollection.Player's snakes & ladders score
     * @param scDice CGCollection.Player's CGCollection.Dice game score
     */
    public Player(String plName, int scOXO, int scSNL, int scDice) {
        playerName = plName;
        scoreOXO = scOXO;
        scoreSNL = scSNL;
        scoreDice = scDice;
    }

    /**
     * Adds three points to a player's noughts & crosses score after a win.
     */
    public void setScoreWinOXO()
    {
        scoreOXO += 3;
    }

    /**
     * Subtracts one point from a player's noughts & crosses score after a loss.
     */
    public void setScoreLoseOXO()
    {
        scoreOXO -= 1;
    }

    /**
     * Gets a player's noughts & crosses score
     *
     * @return Noughts & Crosses Score, integer
     */
    public int getScoreOXO()
    {
        return scoreOXO;
    }

    /**
     * Adds three points to a player's snakes & ladders score after a win.
     */
    public void setScoreWinSNL()
    {
        scoreSNL += 1;
    }

    /**
     * Subtracts one point from a player's snakes & ladders score after a loss.
     */
    public void setScoreLoseSNL()
    {
        scoreSNL -= 1;
    }

    /**
     * Gets a player's snakes & ladders score
     *
     * @return Snakes & Ladders Score, integer
     */
    public int getScoreSNL ()
    {
        return scoreSNL;
    }

    /**
     * Adds three points to a player's dice game score after a win.
     */
    public void setScoreWinDice()
    {
        scoreDice += 3;
    }

    /**
     * Subtracts one point from a player's dice game score after a loss.
     */
    public void setScoreLoseDice()
    {
        scoreDice -= 1;
    }

    /**
     * Gets a player's name.
     * 
     * @return CGCollection.Player's name, string
     */
    public String getPlayerName()
    {
        return playerName;
    }

    /**
     * Gets a player's CGCollection.Dice game score.
     *
     * @return CGCollection.Dice Score, integer
     */
    public int getScoreDice()
    {
        return scoreDice;
    }

    /**
     * Sets a player's board position for Snakes & Ladders
     *
     * @param playerPosition
     */
    public void setBoardPosSNL(int playerPosition)
    {
        boardPosSNL = playerPosition;
    }

    /**
     * Gets a player's board position for Snakes & Ladders
     *
     * @return CGCollection.Player's board position, integer
     */
    public int getBoardPosSNL()
    {
        return boardPosSNL;
    }

    /**
     * Sets a player's CGCollection.Dice Game Score.
     * Used during gameplay to hold the total value of all dice rolled.
     *
     * @param playerScore CGCollection.Player's dice game score, int
     */
    public void setDiceGameScore(int playerScore)
    {
        diceGameScore = playerScore;
    }

    /**
     * Sets a player's CGCollection.Dice Game Score.
     * Used during gameplay to hold the total value of all dice rolled.
     *
     * @return CGCollection.Player's dice game score, int
     */
    public int getDiceGameScore()
    {
        return diceGameScore;
    }
}
