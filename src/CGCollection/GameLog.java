package CGCollection;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Class "CGCollection.GameLog"
 *
 * Holds a log of all games played.
 * Contains timestamp, game type, and information about the winner and loser.
 *
 * @version 1.0
 * @since 2017-12-06
 * @author James Martin
 */

public class GameLog {

    //Variable declarations
    private List<String[]> gameLog;

    /**
     * Constructor
     * @param gLog Game Log, List of string arrays (loaded from file)
     */
    public GameLog(List<String[]> gLog) {
        gameLog = gLog;
    }

    /**
     * Creates a log for the game, and adds to the Game Log.
     *
     * @param gameMode String containing name of game mode
     * @param gameWinner String containing game winner's name
     * @param winnerScore Integer containing winner's score
     * @param gameLoser String containing game loser's name
     * @param loserScore Integer containing loser's score
     */
    public void addGameLog(String gameMode, String gameWinner, int winnerScore, String gameLoser, int loserScore)
    {
        int newLogPos = gameLog.size();
        String gameID = Integer.toString(newLogPos);
        //Get date and time
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String timestamp = String.join(cal.toString(), " ", sdf.toString());
        String wScore = Integer.toString(winnerScore);
        String lScore = Integer.toString(loserScore);

        //Collates strings into array and adds to game log
        String[] game = {gameID, timestamp, gameMode, gameWinner, wScore, gameLoser, lScore};
        gameLog.add(game);

    }

    /**
     * Clears all entries from the game log.
     */
    public void clearGameLog()
    {
        gameLog.clear();
    }

    /**
     * Returns the game log in full.
     * For usage when saving to a file, and displaying on-screen.
     *
     * @return Game log, List of String Arrays
     */
    public List<String[]> getGameLog()
    {

        return gameLog;
    }
}
